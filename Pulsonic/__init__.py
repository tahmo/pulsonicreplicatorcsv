# Module dependancies
import zeep
import xml.etree.ElementTree as ET
import pandas as pd

# Initialize Pulsonic API.
wsdl = 'http://www.pulsoweb.eu/WEBSMETEOVISION_WEB/WebSMeteoVision.awws?wsdl'
client = zeep.Client(wsdl=wsdl)

class apiWrapper(object):
    def setCredentials(self, key, secret, id):
        self.apiKey = key
        self.apiSecret = secret
        self.apiId = id

    def getStations(self):
        connection = client.service.OpenConnection(self.apiKey, self.apiSecret, self.apiId)
        if (connection.body.OpenConnectionResult):
            stationList = client.service.GetStationList()
            return (stationList.body.GetStationListResult).split("\t")
        else:
            raise Exception('WebService connexion successful failed')

    def getMeasurements(self, stationCode, startDate=None, endDate=None, variables=[]):
        connection = client.service.OpenConnection(self.apiKey, self.apiSecret, self.apiId)
        if (connection.body.OpenConnectionResult):
            value = client.service.GetBlockValue(stationCode, ",".join(variables), "H", startDate.strftime('%Y%m%d%H%M%S'), endDate.strftime('%Y%m%d%H%M%S'))
            valueMatrix = []
            xmlResponse = value.body.GetBlockValueResult
            if len(xmlResponse) > 50:
                root = ET.fromstring(value.body.GetBlockValueResult)
                for data in root.findall('LINEDATA'):
                    valueMatrix.append(data.text.split(","))

            if len(valueMatrix):
                df = pd.DataFrame(valueMatrix[::-1], columns=['Timestamp'] + variables, index=pd.DatetimeIndex(list(map(lambda x: pd.Timestamp(x[0]), valueMatrix[::-1]))))
                del df['Timestamp']
                return df
            else :
                return pd.DataFrame()
        else:
            raise Exception('WebService connexion successful failed')


