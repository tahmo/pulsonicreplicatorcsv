# Import dependencies.
import Pulsonic
from utility import columnMapping
import schedule
import datetime
import dateutil
import time
import os
import numpy as np
from models import Base, Station
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

# Load environment variables from file.
from dotenv import load_dotenv
load_dotenv()

# Initialize database session.
db_uri = os.getenv('SQLALCHEMY_DATABASE_URI')
engine = create_engine(os.getenv('DB_URI'))
Session = sessionmaker(bind=engine)

# Create all database tables.
Base.metadata.create_all(engine)

# Initialize Pulsonic API.
api = Pulsonic.apiWrapper()
api.setCredentials(os.getenv('PULSONIC_API_USER'),os.getenv('PULSONIC_API_PASSWORD'),os.getenv('PULSONIC_API_ID'))


def updateMetadata():
 print('Update stations')
 session = Session()
 try:
  # Request station information from API.
  apiStations = api.getStations()

  # Create station objects from retrieved data.
  for stationCode in apiStations:
   if stationCode and stationCode.strip():
    session.merge(Station(
     code=stationCode
    ))
  session.commit()

 except Exception as e:
  print('Error while updating stations: %s' % str(e))
 finally:
  session.close()


def updateMeasurements():
 print('Update measurements')
 session = Session()
 stations = session.query(Station).all()
 session.close()
 for station in stations:
  updateMeasurementsForStation(station)


def updateMeasurementsForStation(station):
 print('Update measurements for station %s' % station.code)
 session = Session()
 try:
  if not os.path.exists('data/%s/' % (station.code)):
   os.makedirs('data/%s/' % (station.code))

  if (station.lastmeasurement):
   startDate = station.lastmeasurement + datetime.timedelta(0, 300)
  else:
   startDate = dateutil.parser.parse(os.getenv('START_TIME'))

  if os.getenv('SPLIT_FILES') == 'day':
   intervalPrefix = startDate.strftime('%Y%m%d')
   startDateInterval = dateutil.parser.parse("%sT00:00:00Z" % intervalPrefix)
   endDateInterval = startDateInterval + dateutil.relativedelta.relativedelta(days=+1) - datetime.timedelta(
    seconds=1)
  else:
   intervalPrefix = startDate.strftime('%Y%m')
   startDateInterval = dateutil.parser.parse("%s01T00:00:00Z" % intervalPrefix)
   endDateInterval = startDateInterval + dateutil.relativedelta.relativedelta(months=+1) - datetime.timedelta(
    seconds=1)

  df = api.getMeasurements(station.code, startDateInterval, endDateInterval, list(columnMapping.keys()))
  if len(df):

   # Static columns.
   if os.getenv('FIXED_COLUMNS') == 'yes':
    for expectedColumn in columnMapping.keys():
     if expectedColumn not in df:
      df[expectedColumn] = np.nan

   # CSV export.
   drop_columns = []
   for columnName in list(df):
    if columnName not in columnMapping:
     drop_columns.append(columnName)
    else:
     df.rename({columnName: columnMapping[columnName]['name']}, axis=1, inplace=True)

   # Drop unused columns.
   df.drop(columns=drop_columns, axis=1, inplace=True)

   # Order columns alphabetically.
   df = df.reindex(columns=sorted(df.columns))

   # Save the dataframe to a CSV file.
   folder = 'data/%s/' % (station.code)
   filePath = '%s/%s_%s.csv' % (folder, station.code, intervalPrefix)
   df.to_csv(filePath, na_rep=os.getenv('NA_VALUE'), date_format=os.getenv('DATE_FORMAT'))

   if os.getenv('CURRENT_FILE_ROWS') != '':
    # Save last rows to the current file.
    df.tail(int(os.getenv('CURRENT_FILE_ROWS'))).to_csv('%s/%s_current.csv' % (folder, station.code),
                                                       na_rep=os.getenv('NA_VALUE'),
                                                       date_format=os.getenv('DATE_FORMAT'))

  if len(df) or datetime.datetime.now(endDateInterval.tzinfo) > endDateInterval:
   print('Updating last measurement data into database')
   session.merge(Station(
    code=station.code,
    lastmeasurement=endDateInterval if (datetime.datetime.now(endDateInterval.tzinfo) > endDateInterval) else datetime.datetime.fromtimestamp(df.tail(1).index.values.item() / 1e9)
   ))
   session.commit()

 except Exception as e:
  print('Error while retrieving measurements for station %s: %s' % (station.code, str(e)))
 finally:
  print('Finished updating measurements for station %s' % station.code)
  session.close()


# Schedule periodic update jobs.
print('Scheduling jobs')
schedule.every(1).hour.at(":40").do(updateMetadata)
schedule.every(1).hour.at(":45").do(updateMeasurements)

# Manually trigger updates for testing purposes.
updateMetadata()
updateMeasurements()

while True:
 schedule.run_pending()
 time.sleep(1)
