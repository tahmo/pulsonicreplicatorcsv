from sqlalchemy import Column, Integer, String, DateTime, Float, ForeignKey, PrimaryKeyConstraint
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class Station(Base):
    """Station information"""
    __tablename__ = "station"

    code = Column(String(50), primary_key=True, nullable=False)
    lastmeasurement = Column(DateTime, nullable=True)
