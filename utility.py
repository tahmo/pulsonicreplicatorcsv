columnMapping = {
    'Temp._mini': {
        'name': 'AirTemperatureMIN',
        'units': 'degrees C'
    },
    'Temp._maxi': {
        'name': 'AirTemperatureMAX',
        'units': 'degrees C'
    },
    'Temp._inst': {
        'name': 'AirTemperatureAVG',
        'units': 'degrees C'
    },
    'Hum._inst': {
        'name': 'RH',
        'units': '%'
    },
    'Cum._pluie': {
        'name': 'Precip',
        'units': 'mm'
    },
    'Ray._total': {
        'name': 'GlobalRadiation',
        'units': 'J/cm2'
    },
    'FF_maxi': {
        'name': 'WindGusts',
        'units': 'm/s'
    },
    'FF_moy': {
        'name': 'WindSpeed',
        'units': 'm/s'
    },
    'Dir._moy': {
        'name': 'WindDirection',
        'units': 'degrees'
    },
    'Pres._inst': {
        'name': 'AtmosphericPressure',
        'units': 'hPa'
    },
    'Pmer': {
        'name': 'AtmosphericPressureMSL',
        'units': 'hPa'
    }
}